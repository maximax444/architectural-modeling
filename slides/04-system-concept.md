# Системная инженерия. <br/> Архитектурное моделирование компьютерных систем

## Лекция 4

### Системная инженерия. Понятие системы и жизненного цикла

Пенской А.В., 2022

----

### План лекции

- Проблемы разработки компьютерных систем
- Коммуникации и поиск решений
- Системная инженерия
- Что такое система?

---

## Проблемы разработки компьютерных систем

> Poor management can increase software costs more rapidly than any other factor. <br/> -- Barry W. Boehm

Ключевые проблемы:

- формулирование/генерация,
- передача/сохранение,
- и использование информации.

А также: неполнота, неоднозначность, нераспределённость, противоречивость, стремление к решению...

----

## Коммуникации и поиск решений

Some of the critical questions for the success of system may be missed.

<div class="row"><div class="col">

- Question is beyond the competence of the developer.
- Template design dominates.
- Artificial narrowing of design requirements.
- Substitution of one task to another.
- Inefficient arrangement of priorities at designing.

</div><div class="col">

### Постановка задачи техническим специалистом

![](figures/communication-problem-senior-and-junior.png)

</div></div>

----

### Попытка разобраться самостоятельно

![](figures/communication-problem-senior-and-junior-detail.png)

----

### Постановка задачи нетехническим специалистом

![](figures/communication-problem-manager-and-dev.png)

---

## Системная инженерия

Системная инженерия (SE)
: междисциплинарный подход и средство, позволяющее реализовать успешные системы. Он фокусируется на целостном и одновременном понимании потребностей заинтересованных сторон (стейкхолдеров); изучении возможностей; документировании требований; и синтезе, проверке, приемке и разработке решений при рассмотрении всей проблемы, от исследования концепции системы до вывода системы из эксплуатации.

--- The Guide to the Systems Engineering Body of Knowledge (SEBoK), V.1.3. 2014.

----

### Предмет системной инженерии

<div class="row"><div class="col">

![](figures/se-domain.png)

</div><div class="col">

![](figures/system-view-of-an-aircraft.png)

</div></div>

----

### Генерация, а не наблюдение

![](figures/reseach-and-engineering-activity.png)

----

### Техническая дисциплина

Management discipline
: organizes the technical effort throughout the system lifecycle, including facilitating collaboration, defining workflows, and deploying development tools.

Technical discipline
: ensures that you rigorously execute a sensible development process, from concept to production to operation.

----

### Роль системного инженера

![](figures/se-role.png)

----

### Активности, связанные с системой

![](figures/system-activities.png)

---

## Классы систем

<div class="row"><div class="col">

![](figures/system-types.png)

</div><div class="col">

- Физические системы
- Кибернетические системы
- Социо системы (soft system)

</div></div>

---

## Сложность

![](figures/complexity.png)

----

| Тип связи       | Отношение              | Масштабный фактор      | Пример                       |
| :-------------- | :--------------------- | ---------------------- | ---------------------------- |
| Топологическая  | Близость               | Распределенность       | Реестр абонентов             |
| Каузальная      | Причина–следствие      | Историчность           | Журнал документооборота      |
| Мереологическая | Часть–целое            | Иерархичность          | Орг. структура               |
| Дескриптивная   | Абстрактное–конкретное | Сложность              | Классификатор видов объектов |
| Телеологическая | Цель–средство          | Многофункциональность  | Реестр оборудования          |

---

## Что такое система?

![](figures/system-is-not-module.png)

---

### Система как совокупность частей

<div class="row"><div class="col">
System
: a combination of interacting elements organized to achieve one or more stated purposes

NOTE A system may be considered as a product or as the services it provides.

</div><div class="col">

![](figures/modeling-complex-system-composition.png)

Так ли это важно?

</div></div>

----

### Нисходящая структура системы

![](figures/system-structure.png)

----

### Модульная структура системы

<div class="row"><div class="col">

![](figures/system-burger.png)

</div><div class="col">

Множество функций

![](figures/system-many-functions.png) <!-- .element height="150px" -->

Множество реализаций

![](figures/system-many-impl.png) <!-- .element height="150px" -->

</div></div>

----

### Модульная иерархия

![](figures/system-module-hier.png)

----

### Точки зрения на структуру

![](figures/system-internal-organisation.png)

*Question*: Какие варианты описания модуля внутри вы знаете?

---

### Операционное окружение

<div class="row"><div class="col">

The environment in which systems are deployed. The problem or opportunity in response to which the system has been developed, exists in this environment.

The operational environment is a significant factor in defining the needed system capabilities, desired stakeholder outcomes and benefits, and constraints.

</div><div class="col">

![](figures/operational-environment-and-enabling-systems.png)

</div></div>

----

#### Идентификация системы

<div class="row"><div class="col">

![](figures/system-idenitity-joke.png)

</div><div class="col">

![](figures/system-id.png)<!-- .element height="220px" -->

![](figures/system-id-iso-81346.png)<!-- .element height="300px" -->

</div></div>

----

#### Stakeholder. Заинтересованная сторона

![](figures/system-stakeholders.png)

----

#### View и Viewpoint

![](figures/system-project-view.png)

---

### Жизненный цикл

Жизненный цикл системы
: эволюция интересующей системы со временем от концепции до вывода из эксплуатации

<div class="row"><div class="col">

![](figures/system-life-cycle-non-formal.png)

</div><div class="col">

![](figures/system-lifecycle-example.png)

</div></div>

----

#### Типовые стадии жизненного цикла

![](figures/system-life-cycle-stages.png)

----

#### Обеспечивающая система

Обеспечивающая система -- система, которая дополняет интересующую систему на этапах ее жизненного цикла, но не обязательно вносит непосредственный вклад в ее функционирование во время эксплуатации.

ПРИМЕЧАНИЕ 1 Например, когда система, представляющая интерес, вступает в стадию производства, требуется вспомогательная производственная система.

ПРИМЕЧАНИЕ 2 Каждая обеспечивающая система имеет свой собственный жизненный цикл. Этот Международный стандарт применим к каждой обеспечивающей системе, когда она сама по себе рассматривается как система, представляющая интерес.

--- ISO 15288

----

![](figures/system-life-cycle.png)

---

### Разработка успешной системы требует

- рассмотрения её структуры
- рассмотрения её операционного окружения
- рассмотрения её жизненного цикла
- рассмотрения обеспечивающих систем

Аналогичный результат получен в: OMG Essence, СМД-методологии.
