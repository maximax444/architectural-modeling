# Системная инженерия

## Архитектурное моделирование компьютерных систем

## Лекция 2

## Предмет моделирования. Сущностная, субстанциональная парадигмы

Пенской А.В., 2022

----

## План лекции

- Предмет моделирования
- Сущностная парадигма
- Субстанциональная парадигма

---

## Проблемы и задачи моделирования

![](figures/geek-and-poke-how-to-create-a-stable-data-model.png)

Кто этим должен заниматься?

----

### Задачи моделирования в рамках компьютерных систем (повтор)

![](figures/traditional-stages-of-system-development.jpg)

- Моделирование предметной области в компьютерной системе.
- Моделирование компьютерной системы в течение её жизненного цикла (концепция, техническое задание, технический проект, архитектура, реализация, целевая система, журналы…).

----

### Рост сложности информационной системы

<div class="row"><div class="col">

Источники сложности:

- предметная область;
- интерфейсы интеграции с другими системами, API;
- особенности реализации (подпорки в терминах Н.Н. Непейводы);
- подмена понятий и объектов.

Object Modelling никак не связано с ООП.

</div><div class="col">

![](figures/boro-increases-in-scope.png)

</div></div>

----

**Пример** из области программирования:

- языки программирования с чётким минималистичным базисом (SmallTalk, lisp, C, Go)
- языки программирования с богатыми возможностями (C++, Scala, PHP, Ruby)

Сводимость всего языка к небольшому **базису**, делает спецификацию на язык компактной и простой, упрощает использование.

**Проблема**: как свести предметную область к небольшому базису в нашем изменчивом мире?

---

## Объекты предметной области

![](figures/boro-focusing-on-things-in-the-business.png)

Выход из неопределённости и множества интерпретаций: сосредоточиться на объектах предметной области.

----

### Конкретные объекты

![](figures/a-particular-table-and-two-particular-chairs.png)

----

### Обобщённые объекты

![](figures/general-types-and-particular-things.png)

----

### Отношения и взаимосвязи

![](figures/general-and-particular-relationships.png)

----

### Изменения и действия

![](figures/gun-trace.jpg)<!-- .element: height="250px" -->

![](figures/transformation.jpg)<!-- .element: height="250px" -->

---

## Сопоставление объектов предметной области и информационной системы

![](figures/boro-data-process-things-changes-mistake.png)<!-- .element: height="175px" -->

<div>

Некорректно, т.к. предметная область и информационная система о разном.

![](figures/boro-account-movements-data-is-change.png)<!-- .element: height="150px" -->

</div> <!-- .element: class="fragment" -->

----

![](figures/boro-data-process-things-changes.png)

---

## Сущностная парадигма. Теория

### (Entity paradigm)

![](figures/plato-raphael.jpg)

- Тип сущности (Entity type)
- Экземпляр сущности (individual entity)
- Атрибут

----

### Миф о пещере

![](figures/plato-cave.jpg)<!-- .element: height="300px" -->

<div class="row"><div class="col">

Сущность -- представление и смысл данной вещи, то, что она есть сама по себе, в отличие от всех других вещей и в отличие от изменчивых (под влиянием тех или иных обстоятельств) состояний.

</div><div class="col">

Каждый объект описывается совокупностью:

- сущность/тип/идея;
- экземпляр/тень/вещь.

</div></div>

----

### Тип сущности и экземпляр сущности

![](figures/boro-entity-individual-entities-belong-to-type.png)

----

### Отличия экземпляров сущностей

<div>

![](figures/boro-entity-attribute.png)

Атрибут -- самостоятельная или подчинённая сущность?

</div> <!-- .element: class="fragment" -->

----

### Сущности и атрибуты

![](figures/boro-entity-atribute-and-entity.png)

----

### Отношения

Представление отношений как в субстанциональной парадигме, через атрибуты. Детали будут позднее.

### Изменения

Представление изменений как в субстанциональной парадигме. Детали будут позднее.

----

#### Повторное использование парадигма-типы-модели

![](figures/boro-entity-reuse.png)

----

#### Повторное использование типы-модели-объекты

![](figures/boro-entity-reuse-in-application.png)

---

## Сущностная парадигма. Практика

### Варианты реализации сущностной парадигмы

![](figures/boro-entity-in-practice.png)

----

### Проблема вариативности моделей предметной области

![](figures/boro-entity-two-format-for-staff.png)

----

### Вариативность табличных представлений

<div class="row"><div class="col">

![](figures/boro-entity-sales-account-table.png)

</div><div class="col">

![](figures/boro-entity-staff-table.png)

</div></div>

----

### Вариативность интерпретации мира

<div class="row"><div class="col">

![](figures/boro-entity-sales-account-and-world.png)

</div><div class="col">

![](figures/boro-entity-staff-and-world.png)

</div></div>

Какой вариант правильный?

Нет неправильного варианта. Есть варианты с разным назначением. <!-- .element: class="fragment" -->

---

## Субстанциональная парадигма

### (Substance paradigm)

![](figures/aristotle-raphael.jpg)

- Вторичная иерархия субстанции (Substance)
- Вторичная иерархия атрибутов

----

<div class="row"><div class="col">

#### Субстанция

Философская категория классической рациональности для обозначения объективной реальности в аспекте внутреннего единства всех форм её проявления и саморазвития. Субстанция неизменна в отличие от перманентно меняющихся свойств и состояний: она есть то, что существует в самой себе и благодаря самой себе.

</div><div class="col">

![](figures/matter-and-form-is-substance.jpg)

Объект описывается:

- совокупностью вторичных субстанций;
- первичной субстанцией.

</div></div>

----

### Переход от сущностной к субстанциональной парадигме

![](figures/boro-entity-to-substance.png)

----

### Вторичная иерархия субстанций

![](figures/boro-substance-hierarchy.png)

----

### Первичная субстанция

![](figures/boro-substance-primary-substance.png)

----

### Иерархия атрибутов

<div class="row"><div class="col">

![](figures/boro-substance-attributes.png)

</div><div class="col">

![](figures/boro-substance-attributes-schema.png)

</div></div>

----

### Отношения между объектами

- тип $<->$ экземпляр -- часть парадигмы;
- сущность $<->$ атрибут -- часть парадигмы;
- сущность $<->$ сущность

<div>

 -- через атрибуты.

![](figures/boro-indidual-relation-by-attribute.png)<!-- .element: height="300px" -->

Проблема: консистентность ссылок.

</div> <!-- .element: class="fragment" -->

----

![](figures/boro-substance-relations-primary-level.png)

---

## Субстанциональная парадигма. Изменения

### Изменение как

<div class="row"><div class="col">

![](figures/boro-substance-secondary-particles.png)

</div><div class="col">

![](figures/boro-substance-primary-levels-of-change.png)

</div></div>

----

![](figures/boro-substance-essential-and-accidental-attributes.png)

----

#### Пример онтогенеза

<!-- ![](figures/caterpillar.png) -->

![](figures/boro-substance-lepidopter.png)<!-- .element: height="250px" -->

![](figures/boro-substance-lepidopter-unchaniging.png)<!-- .element: height="250px" -->

----

#### Процесс

![](figures/boro-substance-arrow-changes.png)

---

## Субстанциональная парадигма. Практика

### Гибкость при моделировании

![](figures/boro-substance-staff.png) <!-- .element: height="300px" -->

В какой технологии мы видели что-то похожее?

Объектно-ориентированное программирование (ООП) <!-- .element: class="fragment" -->

----

### Отображение субстанциональной парадигмы на сущностную

![](figures/boro-map-substance-to-entity.png)

В каком классе ПО это встречается?

Object–Relational Mapping (ORM) <!-- .element: class="fragment" -->

----

#### Выбор уровня

![](figures/boro-substance-to-entiry-level-selection.png)

----

#### Схлопывание уровней

Сверху -- включение субстанции в сущность.

![](figures/boro-substance-to-entiry-level-squize-result.png)

Снизу -- представление субстанции в виде атрибутов.

----

### Проблема иерархии субстанций

![](figures/boro-substance-primary-substance-or-attribute.png)
